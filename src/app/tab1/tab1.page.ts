import { environment } from './../../environments/environment';
import { WeatherService } from './../services/weather.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

  public name = environment.name;
  public data = [];
  private oggi = new Date();
  public today = new Intl.DateTimeFormat('it-IT', { weekday: 'long', month: "long", day: "numeric" }).format(this.oggi); 

  constructor(private weatherService : WeatherService, public modalController: ModalController, private router: Router) {}

  ngOnInit() {
    let reqs = [];
    environment.startingCities.forEach(el => {
      reqs.push(this.weatherService.getWeatherForCity(el));
    });

    forkJoin(reqs).subscribe(
      res => {
        this.data = res;
        console.log('this.data: ', this.data);
      }
    )
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalPage,
      cssClass: 'modal'
    });

    modal.onDidDismiss().then(result=>{
      this.weatherService.getWeatherForCity(result.data).subscribe(
        (c => {
          this.data.push(c);
        })
      )
    })
    return await modal.present();
  }

  public goToTab(param) {
    let navigationExtras: NavigationExtras = {
      state: {
        city: param
      }
    };
    this.router.navigate(['tabs/tab2'], navigationExtras);
  }

}
