import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorModalPage } from '../error-modal/error-modal.page';
import { ModalPage } from '../modal/modal.page';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(public modalController: ModalController) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.log('error: ', error);

          this.modalController.create({
            component: ErrorModalPage,
            componentProps: {
              'code': `${error.status}`,
              'text': "Si è verificato l'errore: "
            },
            cssClass: 'error-modal'
          }).then((res) => {
            res.present();
            res.onDidDismiss().then(action => {
              if (action.data == "ok") {
                
              }
              else {

              }

            });
          });
          return throwError(error.message);
        })
      )
  }


}
