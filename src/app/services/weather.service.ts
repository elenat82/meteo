import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private httpClient: HttpClient) { }

  getWeatherForCity(cityName) {
    return this.httpClient.get(`${environment.corsHack}${environment.currentWeather}?q=${cityName}&appid=${environment.apikey}&units=${environment.units}`);
  }
}
