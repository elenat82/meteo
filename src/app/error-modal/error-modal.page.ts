import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-error-modal',
  templateUrl: './error-modal.page.html',
  styleUrls: ['./error-modal.page.scss'],
})
export class ErrorModalPage implements OnInit {

  @Input() code: string;
  @Input() text: string;

  constructor(public viewCtrl: ModalController) { }

  ngOnInit() {
  }

  close(action) {
    this.viewCtrl.dismiss(action);
  }

}
